/**
 * Windward System Five API
 * Provides Web API functions to Windward System Five
 *
 * OpenAPI spec version: v0.1.3
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.WindwardSystemFiveApi);
  }
}(this, function(expect, WindwardSystemFiveApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new WindwardSystemFiveApi.GetRentalRatesData();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('GetRentalRatesData', function() {
    it('should create an instance of GetRentalRatesData', function() {
      // uncomment below and update the code to test GetRentalRatesData
      //var instane = new WindwardSystemFiveApi.GetRentalRatesData();
      //expect(instance).to.be.a(WindwardSystemFiveApi.GetRentalRatesData);
    });

    it('should have the property partUnique (base name: "PartUnique")', function() {
      // uncomment below and update the code to test the property partUnique
      //var instane = new WindwardSystemFiveApi.GetRentalRatesData();
      //expect(instance).to.be();
    });

    it('should have the property rates (base name: "Rates")', function() {
      // uncomment below and update the code to test the property rates
      //var instane = new WindwardSystemFiveApi.GetRentalRatesData();
      //expect(instance).to.be();
    });

  });

}));
