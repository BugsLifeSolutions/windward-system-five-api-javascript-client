/**
 * Windward System Five API
 * Provides Web API functions to Windward System Five
 *
 * OpenAPI spec version: v0.1.3
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.WindwardSystemFiveApi);
  }
}(this, function(expect, WindwardSystemFiveApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new WindwardSystemFiveApi.FullInvoiceParam();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('FullInvoiceParam', function() {
    it('should create an instance of FullInvoiceParam', function() {
      // uncomment below and update the code to test FullInvoiceParam
      //var instane = new WindwardSystemFiveApi.FullInvoiceParam();
      //expect(instance).to.be.a(WindwardSystemFiveApi.FullInvoiceParam);
    });

    it('should have the property invoiceHeader (base name: "InvoiceHeader")', function() {
      // uncomment below and update the code to test the property invoiceHeader
      //var instane = new WindwardSystemFiveApi.FullInvoiceParam();
      //expect(instance).to.be();
    });

    it('should have the property invoiceLines (base name: "InvoiceLines")', function() {
      // uncomment below and update the code to test the property invoiceLines
      //var instane = new WindwardSystemFiveApi.FullInvoiceParam();
      //expect(instance).to.be();
    });

    it('should have the property invoiceTenders (base name: "InvoiceTenders")', function() {
      // uncomment below and update the code to test the property invoiceTenders
      //var instane = new WindwardSystemFiveApi.FullInvoiceParam();
      //expect(instance).to.be();
    });

    it('should have the property invoiceShipping (base name: "InvoiceShipping")', function() {
      // uncomment below and update the code to test the property invoiceShipping
      //var instane = new WindwardSystemFiveApi.FullInvoiceParam();
      //expect(instance).to.be();
    });

    it('should have the property invoiceBilling (base name: "InvoiceBilling")', function() {
      // uncomment below and update the code to test the property invoiceBilling
      //var instane = new WindwardSystemFiveApi.FullInvoiceParam();
      //expect(instance).to.be();
    });

  });

}));
