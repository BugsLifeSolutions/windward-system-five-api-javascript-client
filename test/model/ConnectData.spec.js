/**
 * Windward System Five API
 * Provides Web API functions to Windward System Five
 *
 * OpenAPI spec version: v0.1.3
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.WindwardSystemFiveApi);
  }
}(this, function(expect, WindwardSystemFiveApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new WindwardSystemFiveApi.ConnectData();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('ConnectData', function() {
    it('should create an instance of ConnectData', function() {
      // uncomment below and update the code to test ConnectData
      //var instane = new WindwardSystemFiveApi.ConnectData();
      //expect(instance).to.be.a(WindwardSystemFiveApi.ConnectData);
    });

    it('should have the property response (base name: "Response")', function() {
      // uncomment below and update the code to test the property response
      //var instane = new WindwardSystemFiveApi.ConnectData();
      //expect(instance).to.be();
    });

    it('should have the property companyName (base name: "CompanyName")', function() {
      // uncomment below and update the code to test the property companyName
      //var instane = new WindwardSystemFiveApi.ConnectData();
      //expect(instance).to.be();
    });

    it('should have the property serial (base name: "Serial")', function() {
      // uncomment below and update the code to test the property serial
      //var instane = new WindwardSystemFiveApi.ConnectData();
      //expect(instance).to.be();
    });

    it('should have the property department (base name: "Department")', function() {
      // uncomment below and update the code to test the property department
      //var instane = new WindwardSystemFiveApi.ConnectData();
      //expect(instance).to.be();
    });

    it('should have the property applicationVersion (base name: "ApplicationVersion")', function() {
      // uncomment below and update the code to test the property applicationVersion
      //var instane = new WindwardSystemFiveApi.ConnectData();
      //expect(instance).to.be();
    });

    it('should have the property dataVersion (base name: "DataVersion")', function() {
      // uncomment below and update the code to test the property dataVersion
      //var instane = new WindwardSystemFiveApi.ConnectData();
      //expect(instance).to.be();
    });

  });

}));
