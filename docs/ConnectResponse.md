# WindwardSystemFiveApi.ConnectResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[ConnectData]**](ConnectData.md) |  | [optional] 


