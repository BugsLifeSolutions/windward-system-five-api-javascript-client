# WindwardSystemFiveApi.InsertFullInvoiceResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[InsertFullInvoiceResult]**](InsertFullInvoiceResult.md) |  | [optional] 


