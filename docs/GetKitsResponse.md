# WindwardSystemFiveApi.GetKitsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetKitsResult]**](GetKitsResult.md) |  | [optional] 


