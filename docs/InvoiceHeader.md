# WindwardSystemFiveApi.InvoiceHeader

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoiceDate** | **String** |  | 
**invoiceType** | **String** |  | 
**invoiceSubType** | **String** |  | [optional] 
**invoiceDepartment** | **Number** |  | 
**invoiceBookMonth** | **String** |  | 
**invoiceCustomer** | **Number** |  | 
**invoiceSalesman** | **Number** |  | [optional] 
**invoiceComment** | **String** |  | [optional] 


