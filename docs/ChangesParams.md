# WindwardSystemFiveApi.ChangesParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**effectiveDate** | **Date** |  | 
**pageSize** | **Number** |  | 
**pageNumber** | **Number** |  | 


