# WindwardSystemFiveApi.GetStockChangesData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partUnique** | **Number** |  | [optional] 
**effectiveDate** | **String** |  | [optional] 
**operationState** | **String** |  | [optional] 


