# WindwardSystemFiveApi.GetCategoriesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetCategoriesResult]**](GetCategoriesResult.md) |  | [optional] 


