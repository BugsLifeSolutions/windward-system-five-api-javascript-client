# WindwardSystemFiveApi.GetPartPriceSchedule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_number** | **Number** |  | [optional] 
**priceDept** | **Number** |  | [optional] 
**sType** | **String** |  | [optional] 
**sTypeSale** | **String** |  | [optional] 
**schedule** | **Number** |  | [optional] 
**scheduleSale** | **Number** |  | [optional] 
**price** | **Number** |  | [optional] 
**priceSale** | **Number** |  | [optional] 
**quanDisc** | **Number** |  | [optional] 


