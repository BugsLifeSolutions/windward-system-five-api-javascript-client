# WindwardSystemFiveApi.InsertPartsParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parts** | [**[InsertPartsParamsData]**](InsertPartsParamsData.md) |  | 


