# WindwardSystemFiveApi.GetDepartmentsData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**departmentNumber** | **Number** |  | [optional] 
**departmentName** | **String** |  | [optional] 


