# WindwardSystemFiveApi.GetSuppliersData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierUnique** | **Number** |  | [optional] 
**searchContact** | **Number** |  | [optional] 
**supplierName** | **String** |  | [optional] 
**addr1** | **String** |  | [optional] 
**addr2** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**postal** | **String** |  | [optional] 
**state** | **String** |  | [optional] 
**country** | **String** |  | [optional] 
**phone1** | **String** |  | [optional] 
**phone2** | **String** |  | [optional] 
**phone3** | **String** |  | [optional] 
**aPDiscount** | **Number** |  | [optional] 
**aPDiscountRule** | **Number** |  | [optional] 
**aPDiscountDays** | **Number** |  | [optional] 
**balances** | [**GetSuppliersBalancesData**](GetSuppliersBalancesData.md) |  | [optional] 
**duty** | **String** |  | [optional] 
**fedTax** | **String** |  | [optional] 
**autoDiscount** | **String** |  | [optional] 
**dueDays** | **Number** |  | [optional] 
**priceSchedule** | **Number** |  | [optional] 
**contractDate** | **Date** |  | [optional] 
**salesman** | **String** |  | [optional] 
**cSType** | **String** |  | [optional] 
**terms** | **String** |  | [optional] 
**interest** | **Number** |  | [optional] 
**taxStatus** | **String** |  | [optional] 
**taxNumber** | **String** |  | [optional] 
**bankInfo** | **String** |  | [optional] 
**shipNo** | **Number** |  | [optional] 
**creditLimit** | **Number** |  | [optional] 
**standingPO** | **String** |  | [optional] 
**pOExpiryDate** | **Date** |  | [optional] 
**pOMaximumValue** | **Number** |  | [optional] 
**pOBilledSoFar** | **Number** |  | [optional] 
**gSTExempt** | **String** |  | [optional] 
**department** | **Number** |  | [optional] 
**taxCode** | **String** |  | [optional] 
**_number** | **String** |  | [optional] 
**disNv** | **Number** |  | [optional] 
**disStat** | **Number** |  | [optional] 
**retailType** | **String** |  | [optional] 
**foreign** | **String** |  | [optional] 
**lastVisit** | **Date** |  | [optional] 
**eMail** | **String** |  | [optional] 
**web** | **String** |  | [optional] 
**passWord** | **String** |  | [optional] 
**eFTAccount** | **String** |  | [optional] 
**eFTBank** | **String** |  | [optional] 
**eFTName** | **String** |  | [optional] 
**timeZoneID** | **String** |  | [optional] 
**warningComments** | **String** |  | [optional] 
**freeFormComments** | **String** |  | [optional] 
**lkupWords** | **String** |  | [optional] 


