# WindwardSystemFiveApi.ListSuppliersResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[ListSuppliersResult]**](ListSuppliersResult.md) |  | [optional] 


