# WindwardSystemFiveApi.GetSupersedingPartsResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[GetSupersedingPartsData]**](GetSupersedingPartsData.md) |  | [optional] 
**response** | **String** |  | [optional] 
**reason** | **String** |  | [optional] 
**stopwatch** | **String** |  | [optional] 


