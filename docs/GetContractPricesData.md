# WindwardSystemFiveApi.GetContractPricesData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerID** | **Number** |  | [optional] 
**partID** | **Number** |  | [optional] 
**price** | **Number** |  | [optional] 


