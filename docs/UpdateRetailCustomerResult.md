# WindwardSystemFiveApi.UpdateRetailCustomerResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[UpdateRetailCustomerData]**](UpdateRetailCustomerData.md) |  | [optional] 
**response** | **String** |  | [optional] 
**reason** | **String** |  | [optional] 
**stopwatch** | **String** |  | [optional] 


