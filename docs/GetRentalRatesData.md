# WindwardSystemFiveApi.GetRentalRatesData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partUnique** | **Number** |  | [optional] 
**rates** | [**[GetRentalRatesDataRate]**](GetRentalRatesDataRate.md) |  | 


