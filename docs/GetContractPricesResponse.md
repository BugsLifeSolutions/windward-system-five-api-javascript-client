# WindwardSystemFiveApi.GetContractPricesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetContractPricesResult]**](GetContractPricesResult.md) |  | [optional] 


