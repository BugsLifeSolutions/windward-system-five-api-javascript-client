# WindwardSystemFiveApi.GetSupersedingPartsChangesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetSupersedingPartsChangesResult]**](GetSupersedingPartsChangesResult.md) |  | [optional] 


