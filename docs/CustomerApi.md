# WindwardSystemFiveApi.CustomerApi

All URIs are relative to *https://api.applianceshack.com:2143/datasnap/rest/TServerMethodsWebAPI*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCustomerChanges**](CustomerApi.md#getCustomerChanges) | **POST** /Get_Customer_Changes | 
[**getCustomers**](CustomerApi.md#getCustomers) | **POST** /Get_Customers | 
[**getCustomersByEmail**](CustomerApi.md#getCustomersByEmail) | **POST** /Get_Customer_By_Email | 
[**insertRetailCustomer**](CustomerApi.md#insertRetailCustomer) | **POST** /Insert_Retail_Customer | 
[**listCustomers**](CustomerApi.md#listCustomers) | **POST** /List_Customers | 
[**updateRetailCustomer**](CustomerApi.md#updateRetailCustomer) | **POST** /Update_Retail_Customer | 


<a name="getCustomerChanges"></a>
# **getCustomerChanges**
> GetCustomersResponse getCustomerChanges(getCustomerChanges)



Accepts a Changes_Params object. If successful, returns an GetCustomers_Response.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.CustomerApi();

var getCustomerChanges = new WindwardSystemFiveApi.ChangesParams(); // ChangesParams | Search parameters


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getCustomerChanges(getCustomerChanges, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getCustomerChanges** | [**ChangesParams**](ChangesParams.md)| Search parameters | 

### Return type

[**GetCustomersResponse**](GetCustomersResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCustomers"></a>
# **getCustomers**
> GetCustomersResponse getCustomers(getCustomers)



Accepts a FieldsFilters_Params object. If successful, returns an GetCustomers_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.CustomerApi();

var getCustomers = new WindwardSystemFiveApi.FieldsFiltersParams(); // FieldsFiltersParams | Customer Field/Filter params


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getCustomers(getCustomers, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getCustomers** | [**FieldsFiltersParams**](FieldsFiltersParams.md)| Customer Field/Filter params | 

### Return type

[**GetCustomersResponse**](GetCustomersResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCustomersByEmail"></a>
# **getCustomersByEmail**
> ListCustomersResponse getCustomersByEmail(getCustomerByEmail)



Accepts a Email_Param object. If successful, returns an ListCustomers_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.CustomerApi();

var getCustomerByEmail = new WindwardSystemFiveApi.EmailParam(); // EmailParam | Email Address


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getCustomersByEmail(getCustomerByEmail, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getCustomerByEmail** | [**EmailParam**](EmailParam.md)| Email Address | 

### Return type

[**ListCustomersResponse**](ListCustomersResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="insertRetailCustomer"></a>
# **insertRetailCustomer**
> InsertRetailCustomerResponse insertRetailCustomer(insertRetailCustomer)



Specialized version of Insert_Customer that accepts both FirstName and LastName parameters and uses them to properly format the Business Name property in System Five. If successful, returns an InsertRetailCustomer_Response.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.CustomerApi();

var insertRetailCustomer = new WindwardSystemFiveApi.InsertRetailCustomerParams(); // InsertRetailCustomerParams | Search parameters


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.insertRetailCustomer(insertRetailCustomer, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **insertRetailCustomer** | [**InsertRetailCustomerParams**](InsertRetailCustomerParams.md)| Search parameters | 

### Return type

[**InsertRetailCustomerResponse**](InsertRetailCustomerResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="listCustomers"></a>
# **listCustomers**
> ListCustomersResponse listCustomers(listCustomers)



Accepts a CustomerUniques_Param object. If successful, returns an ListCustomers_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.CustomerApi();

var listCustomers = new WindwardSystemFiveApi.CustomerUniquesParam(); // CustomerUniquesParam | Customer Unique Number(s)


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.listCustomers(listCustomers, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **listCustomers** | [**CustomerUniquesParam**](CustomerUniquesParam.md)| Customer Unique Number(s) | 

### Return type

[**ListCustomersResponse**](ListCustomersResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateRetailCustomer"></a>
# **updateRetailCustomer**
> UpdateRetailCustomerResponse updateRetailCustomer(updateRetailCustomer)



Specialized version of Update_Customer that accepts both FirstName and LastName parameters and uses them to properly format the Business Name property in System Five. If successful, returns an UpdateRetailCustomer_Response.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.CustomerApi();

var updateRetailCustomer = new WindwardSystemFiveApi.UpdateRetailCustomerParams(); // UpdateRetailCustomerParams | Search parameters


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateRetailCustomer(updateRetailCustomer, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateRetailCustomer** | [**UpdateRetailCustomerParams**](UpdateRetailCustomerParams.md)| Search parameters | 

### Return type

[**UpdateRetailCustomerResponse**](UpdateRetailCustomerResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

