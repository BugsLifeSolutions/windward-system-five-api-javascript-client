# WindwardSystemFiveApi.GetSupersedingPartsData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partUnique** | **Number** |  | [optional] 
**supersedingPartUnique** | **Number** |  | [optional] 
**supersedingType** | **String** |  | [optional] 


