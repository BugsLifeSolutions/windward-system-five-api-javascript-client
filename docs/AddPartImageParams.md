# WindwardSystemFiveApi.AddPartImageParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partUnique** | **Number** |  | 
**imageURL** | **String** |  | 
**imageFileName** | **String** |  | 


