# WindwardSystemFiveApi.GetContractPricesParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerID** | **Number** |  | 
**partUniques** | **[Number]** |  | [optional] 


