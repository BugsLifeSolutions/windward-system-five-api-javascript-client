# WindwardSystemFiveApi.UpdateRetailCustomerResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[UpdateRetailCustomerResult]**](UpdateRetailCustomerResult.md) |  | [optional] 


