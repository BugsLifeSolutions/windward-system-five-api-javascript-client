# WindwardSystemFiveApi.GetSupersedingPartsChangesData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partUnique** | **Number** |  | [optional] 
**supersedingPartUnique** | **Number** |  | [optional] 
**effectiveDate** | **String** |  | [optional] 
**operationState** | **String** |  | [optional] 


