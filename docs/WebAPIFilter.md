# WindwardSystemFiveApi.WebAPIFilter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | **String** |  | 
**operator** | **String** |  | 
**value** | **String** |  | 


