# WindwardSystemFiveApi.HandshakeResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[HandshakeData]**](HandshakeData.md) |  | [optional] 


