# WindwardSystemFiveApi.GetSuppliersBalancesData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**S_BALANCE** | **Number** |  | [optional] 
**S_ADVANCE** | **Number** |  | [optional] 
**sSB30** | **Number** |  | [optional] 
**sSB60** | **Number** |  | [optional] 
**sSB90** | **Number** |  | [optional] 


