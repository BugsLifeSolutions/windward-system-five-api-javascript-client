# WindwardSystemFiveApi.ListSuppliersResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[ListSuppliersData]**](ListSuppliersData.md) |  | [optional] 
**response** | **String** |  | [optional] 
**reason** | **String** |  | [optional] 
**stopwatch** | **String** |  | [optional] 


