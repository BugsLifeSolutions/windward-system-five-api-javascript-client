# WindwardSystemFiveApi.GetStockResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetStockResult]**](GetStockResult.md) |  | [optional] 


