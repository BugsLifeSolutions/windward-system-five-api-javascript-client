# WindwardSystemFiveApi.UpdateRetailCustomerData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | [**CustomerDataAccount**](CustomerDataAccount.md) |  | [optional] 
**currentTime** | **Number** |  | [optional] 
**currentDate** | **Date** |  | [optional] 


