# WindwardSystemFiveApi.GetPartPricesData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partUnique** | **Number** |  | [optional] 
**effectiveDate** | **String** |  | [optional] 
**listPriceSchedule** | **Number** |  | [optional] 
**isOnSale** | **Boolean** |  | [optional] 
**priceSchedule0** | [**PartPriceData**](PartPriceData.md) |  | [optional] 
**priceSchedule1** | [**PartPriceData**](PartPriceData.md) |  | [optional] 
**priceSchedule_** | [**PartPriceData**](PartPriceData.md) |  | [optional] 
**priceSchedule99** | [**PartPriceData**](PartPriceData.md) |  | [optional] 


