# WindwardSystemFiveApi.RentalsAvailabilityParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parts** | **[String]** |  | 
**filters** | [**[WebAPIFilter]**](WebAPIFilter.md) |  | 
**startDate** | **String** |  | 
**endDate** | **String** |  | 


