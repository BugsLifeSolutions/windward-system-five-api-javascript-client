# WindwardSystemFiveApi.InsertRetailCustomerResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[InsertRetailCustomerResult]**](InsertRetailCustomerResult.md) |  | [optional] 


