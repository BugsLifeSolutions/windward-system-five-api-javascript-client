# WindwardSystemFiveApi.InsertFullInvoiceResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[InsertFullInvoiceData]**](InsertFullInvoiceData.md) |  | [optional] 
**response** | **String** |  | [optional] 
**reason** | **String** |  | [optional] 
**stopwatch** | **String** |  | [optional] 


