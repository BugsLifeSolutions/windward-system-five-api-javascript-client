# WindwardSystemFiveApi.GetMainCategoriesData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_number** | **Number** |  | [optional] 
**mainName** | **String** |  | [optional] 
**catStart** | **Number** |  | [optional] 
**catEnd** | **Number** |  | [optional] 


