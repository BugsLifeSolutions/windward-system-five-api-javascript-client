# WindwardSystemFiveApi.GetRentalRatesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetRentalRatesResult]**](GetRentalRatesResult.md) |  | [optional] 


