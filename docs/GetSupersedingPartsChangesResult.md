# WindwardSystemFiveApi.GetSupersedingPartsChangesResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[GetSupersedingPartsChangesData]**](GetSupersedingPartsChangesData.md) |  | [optional] 
**response** | **String** |  | [optional] 
**reason** | **String** |  | [optional] 
**stopwatch** | **String** |  | [optional] 


