# WindwardSystemFiveApi.InsertRetailCustomerParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstName** | **String** |  | 
**lastName** | **String** |  | 
**addr1** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**postal** | **String** |  | [optional] 
**state** | **String** |  | [optional] 
**country** | **String** |  | [optional] 
**phone1** | **String** |  | 
**phone2** | **String** |  | [optional] 
**phone3** | **String** |  | [optional] 
**aPDiscount** | **Number** |  | [optional] 
**aPDiscountRule** | **Number** |  | [optional] 
**aPDiscountDays** | **String** |  | [optional] 
**duty** | **String** |  | [optional] 
**fedTax** | **String** |  | [optional] 
**autoDiscount** | **String** |  | [optional] 
**dueDays** | **Number** |  | [optional] 
**priceSchedule** | **Number** |  | [optional] 
**salesman** | **Number** |  | [optional] 
**cSType** | **String** |  | [optional] 
**interest** | **Number** |  | [optional] 
**taxStatus** | **String** |  | [optional] 
**shipNo** | **Number** |  | [optional] 
**creditLimit** | **Number** |  | [optional] 
**pOMaximumValue** | **Number** |  | [optional] 
**pOBilledSoFar** | **Number** |  | [optional] 
**gSTExempt** | **String** |  | [optional] 
**department** | **Number** |  | [optional] 
**disNv** | **Number** |  | [optional] 
**disStat** | **Number** |  | [optional] 
**foreign** | **String** |  | [optional] 
**lastVisit** | **Date** |  | [optional] 


