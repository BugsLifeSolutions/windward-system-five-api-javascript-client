# WindwardSystemFiveApi.InsertPartsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[InsertPartsResult]**](InsertPartsResult.md) |  | [optional] 


