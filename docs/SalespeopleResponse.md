# WindwardSystemFiveApi.SalespeopleResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[SalespeopleResult]**](SalespeopleResult.md) |  | [optional] 


