# WindwardSystemFiveApi.GetSuppliersResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetSuppliersResult]**](GetSuppliersResult.md) |  | [optional] 


