# WindwardSystemFiveApi.ListPartsResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[ListPartsData]**](ListPartsData.md) |  | [optional] 
**response** | **String** |  | [optional] 
**reason** | **String** |  | [optional] 
**stopwatch** | **String** |  | [optional] 


