# WindwardSystemFiveApi.InvoiceLine

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partUnique** | **Number** |  | 
**ordered** | **Number** |  | 
**price** | **Number** |  | [optional] 
**description** | **String** |  | [optional] 


