# WindwardSystemFiveApi.InsertPartsParamsData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **String** |  | 
**part** | **String** |  | 
**size1** | **String** |  | [optional] 
**size2** | **String** |  | [optional] 
**size3** | **String** |  | [optional] 
**supplierPartNumber** | **String** |  | [optional] 
**description** | **String** |  | 
**description2** | **String** |  | [optional] 
**saleStart** | **Date** |  | [optional] 
**saleEnd** | **Date** |  | [optional] 
**country** | **String** |  | [optional] 
**weight** | **Number** |  | [optional] 
**footage** | **Number** |  | [optional] 
**serial** | **String** |  | [optional] 
**requiresE4473** | **String** |  | [optional] 
**sellWeight** | **String** |  | [optional] 
**volume** | **Number** |  | [optional] 
**measurement1** | **Number** |  | [optional] 
**measurement2** | **Number** |  | [optional] 
**eCommerce** | **String** |  | [optional] 
**markedAsDeleted** | **String** |  | [optional] 
**sellingComments** | **String** |  | [optional] 
**purchaseComments** | **String** |  | [optional] 
**webComments** | **String** |  | [optional] 
**freeFormComments** | **String** |  | [optional] 
**lkupWords** | **String** |  | [optional] 


