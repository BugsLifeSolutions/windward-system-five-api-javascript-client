# WindwardSystemFiveApi.PartPriceData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**priceSchedule** | **Number** |  | [optional] 
**regularPrice** | **Number** |  | [optional] 
**salePrice** | **Number** |  | [optional] 
**quantityDiscount** | **Boolean** |  | [optional] 


