# WindwardSystemFiveApi.InvoiceApi

All URIs are relative to *https://api.applianceshack.com:2143/datasnap/rest/TServerMethodsWebAPI*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTenderTypes**](InvoiceApi.md#getTenderTypes) | **GET** /Get_Tender_Types | 
[**insertFullInvoice**](InvoiceApi.md#insertFullInvoice) | **POST** /Insert_Full_Invoice | 


<a name="getTenderTypes"></a>
# **getTenderTypes**
> TenderTypesResponse getTenderTypes()



Returns a list of valid Tender Types to be used with Insert_Full_Invoice() method.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InvoiceApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getTenderTypes(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**TenderTypesResponse**](TenderTypesResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="insertFullInvoice"></a>
# **insertFullInvoice**
> InsertFullInvoiceResponse insertFullInvoice(insertFullInvoice)



Accepts a FullInvoice_Param object. If successful, returns an InsertFullInvoice_Response object containing the details of the newly created Invoice.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InvoiceApi();

var insertFullInvoice = new WindwardSystemFiveApi.FullInvoiceParam(); // FullInvoiceParam | Invoice to Insert


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.insertFullInvoice(insertFullInvoice, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **insertFullInvoice** | [**FullInvoiceParam**](FullInvoiceParam.md)| Invoice to Insert | 

### Return type

[**InsertFullInvoiceResponse**](InsertFullInvoiceResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

