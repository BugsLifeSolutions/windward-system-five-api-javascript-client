# WindwardSystemFiveApi.GetPartPriceChangesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetPartPriceChangesResult]**](GetPartPriceChangesResult.md) |  | [optional] 


