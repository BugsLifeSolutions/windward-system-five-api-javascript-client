# WindwardSystemFiveApi.AddPartImageData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partUnique** | **Number** |  | [optional] 
**imageFileName** | **Number** |  | [optional] 


