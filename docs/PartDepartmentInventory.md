# WindwardSystemFiveApi.PartDepartmentInventory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**departmentNumber** | **Number** |  | [optional] 
**quan** | **Number** |  | [optional] 
**WO** | **Number** |  | [optional] 
**order** | **Number** |  | [optional] 
**layaway** | **Number** |  | [optional] 
**specialOrder** | **Number** |  | [optional] 
**bkOrder** | **Number** |  | [optional] 
**rMAOut** | **Number** |  | [optional] 
**rMAIn** | **Number** |  | [optional] 
**hold** | **Number** |  | [optional] 
**holdWO** | **Number** |  | [optional] 
**sOStock** | **Number** |  | [optional] 
**inStock** | **Number** |  | [optional] 
**trackQuantity** | **Boolean** |  | [optional] 
**saleStart** | **String** |  | [optional] 
**saleEnd** | **String** |  | [optional] 
**defaultPriceLevel** | **Number** |  | [optional] 
**cashPriceLevel** | **Number** |  | [optional] 
**listPriceLevel** | **Number** |  | [optional] 
**rentalDays** | **Number** |  | [optional] 
**rentalRevenue** | **Number** |  | [optional] 
**stock** | **String** |  | [optional] 


