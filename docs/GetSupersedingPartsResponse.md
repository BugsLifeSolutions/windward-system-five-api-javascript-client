# WindwardSystemFiveApi.GetSupersedingPartsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetSupersedingPartsResult]**](GetSupersedingPartsResult.md) |  | [optional] 


