# WindwardSystemFiveApi.RentalRatesParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partUniques** | **[String]** |  | 
**department** | **Number** |  | 


