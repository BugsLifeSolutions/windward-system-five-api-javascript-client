# WindwardSystemFiveApi.ConnectData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**response** | **String** |  | [optional] 
**companyName** | **String** |  | [optional] 
**serial** | **String** |  | [optional] 
**department** | **String** |  | [optional] 
**applicationVersion** | **String** |  | [optional] 
**dataVersion** | **String** |  | [optional] 


