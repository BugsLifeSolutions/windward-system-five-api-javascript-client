# WindwardSystemFiveApi.GetPartsParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields** | **[String]** |  | 
**filters** | [**[WebAPIFilter]**](WebAPIFilter.md) |  | 
**pageSize** | **Number** |  | [optional] 
**pageNumber** | **Number** |  | [optional] 


