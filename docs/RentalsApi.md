# WindwardSystemFiveApi.RentalsApi

All URIs are relative to *https://api.applianceshack.com:2143/datasnap/rest/TServerMethodsWebAPI*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getRentalRates**](RentalsApi.md#getRentalRates) | **POST** /Get_Rental_Rates | 
[**getRentalsAvailability**](RentalsApi.md#getRentalsAvailability) | **POST** /Get_Rentals_Availability | 


<a name="getRentalRates"></a>
# **getRentalRates**
> GetRentalRatesResponse getRentalRates(getRentalRates)



Accepts a RentalRates_Params object. If successful, returns an GetRentalRates_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.RentalsApi();

var getRentalRates = new WindwardSystemFiveApi.RentalRatesParams(); // RentalRatesParams | Search parameters


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getRentalRates(getRentalRates, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getRentalRates** | [**RentalRatesParams**](RentalRatesParams.md)| Search parameters | 

### Return type

[**GetRentalRatesResponse**](GetRentalRatesResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getRentalsAvailability"></a>
# **getRentalsAvailability**
> GetRentalsAvailabilityResponse getRentalsAvailability(getRentalsAvailability)



Accepts a RentalsAvailability_Params object. If successful, returns an GetRentalsAvailability_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.RentalsApi();

var getRentalsAvailability = new WindwardSystemFiveApi.RentalsAvailabilityParams(); // RentalsAvailabilityParams | Search parameters


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getRentalsAvailability(getRentalsAvailability, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getRentalsAvailability** | [**RentalsAvailabilityParams**](RentalsAvailabilityParams.md)| Search parameters | 

### Return type

[**GetRentalsAvailabilityResponse**](GetRentalsAvailabilityResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

