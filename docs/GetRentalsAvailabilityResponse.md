# WindwardSystemFiveApi.GetRentalsAvailabilityResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetRentalsAvailabilityResult]**](GetRentalsAvailabilityResult.md) |  | [optional] 


