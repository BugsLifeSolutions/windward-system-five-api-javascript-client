# WindwardSystemFiveApi.PartPricesParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partUniques** | **[Number]** |  | 
**effectiveDate** | **Date** |  | 


