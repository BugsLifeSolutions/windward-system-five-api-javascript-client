# WindwardSystemFiveApi.SuppliersApi

All URIs are relative to *https://api.applianceshack.com:2143/datasnap/rest/TServerMethodsWebAPI*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getSuppliers**](SuppliersApi.md#getSuppliers) | **POST** /Get_Suppliers | 
[**listSuppliers**](SuppliersApi.md#listSuppliers) | **POST** /List_Suppliers | 


<a name="getSuppliers"></a>
# **getSuppliers**
> GetSuppliersResponse getSuppliers(getSuppliers)



Accepts a FieldsFilters_Params object. If successful, returns an GetSuppliers_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.SuppliersApi();

var getSuppliers = new WindwardSystemFiveApi.FieldsFiltersParams(); // FieldsFiltersParams | Part Field/Filter params


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getSuppliers(getSuppliers, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getSuppliers** | [**FieldsFiltersParams**](FieldsFiltersParams.md)| Part Field/Filter params | 

### Return type

[**GetSuppliersResponse**](GetSuppliersResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="listSuppliers"></a>
# **listSuppliers**
> ListSuppliersResponse listSuppliers(listSuppliers)



Accepts a SupplierUniques_Param object. If successful, returns an ListSuppliers_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.SuppliersApi();

var listSuppliers = new WindwardSystemFiveApi.SupplierUniquesParam(); // SupplierUniquesParam | Supplier Unique Number(s)


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.listSuppliers(listSuppliers, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **listSuppliers** | [**SupplierUniquesParam**](SupplierUniquesParam.md)| Supplier Unique Number(s) | 

### Return type

[**ListSuppliersResponse**](ListSuppliersResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

