# WindwardSystemFiveApi.GetMasterContractResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetMasterContractResult]**](GetMasterContractResult.md) |  | [optional] 


