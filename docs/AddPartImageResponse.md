# WindwardSystemFiveApi.AddPartImageResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[AddPartImageResult]**](AddPartImageResult.md) |  | [optional] 


