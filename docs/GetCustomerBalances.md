# WindwardSystemFiveApi.GetCustomerBalances

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**C_BALANCE** | **Number** |  | [optional] 
**C_DEPOSIT** | **Number** |  | [optional] 
**cSB30** | **Number** |  | [optional] 
**cSB60** | **Number** |  | [optional] 
**cSB90** | **Number** |  | [optional] 
**CN_BALANCE** | **Number** |  | [optional] 
**CN_DEPOSIT** | **Number** |  | [optional] 
**C_PO_BILLED** | **Number** |  | [optional] 


