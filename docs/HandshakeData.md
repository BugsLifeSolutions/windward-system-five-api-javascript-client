# WindwardSystemFiveApi.HandshakeData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**systemFiveWebAPI** | **String** |  | [optional] 
**version** | **String** |  | [optional] 


