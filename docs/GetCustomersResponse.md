# WindwardSystemFiveApi.GetCustomersResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetCustomersResult]**](GetCustomersResult.md) |  | [optional] 


