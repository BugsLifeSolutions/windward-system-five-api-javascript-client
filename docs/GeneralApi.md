# WindwardSystemFiveApi.GeneralApi

All URIs are relative to *https://api.applianceshack.com:2143/datasnap/rest/TServerMethodsWebAPI*

Method | HTTP request | Description
------------- | ------------- | -------------
[**connect**](GeneralApi.md#connect) | **GET** /Connect | 
[**getDepartments**](GeneralApi.md#getDepartments) | **GET** /Get_Departments | 
[**getSalespeople**](GeneralApi.md#getSalespeople) | **GET** /Get_Salespeople | 
[**handshake**](GeneralApi.md#handshake) | **GET** /Handshake | 


<a name="connect"></a>
# **connect**
> ConnectResponse connect()



Returns API info.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.GeneralApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.connect(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ConnectResponse**](ConnectResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getDepartments"></a>
# **getDepartments**
> GetDepartmentsResponse getDepartments()



Returns a list of Departments.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.GeneralApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getDepartments(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**GetDepartmentsResponse**](GetDepartmentsResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getSalespeople"></a>
# **getSalespeople**
> SalespeopleResponse getSalespeople()



Returns a list of valid Salespeople.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.GeneralApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getSalespeople(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SalespeopleResponse**](SalespeopleResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="handshake"></a>
# **handshake**
> HandshakeResponse handshake()



Returns basic API info.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.GeneralApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.handshake(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**HandshakeResponse**](HandshakeResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

