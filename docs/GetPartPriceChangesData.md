# WindwardSystemFiveApi.GetPartPriceChangesData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partPriceUnique** | **Number** |  | [optional] 
**partUnique** | **Number** |  | [optional] 
**priceScheduleLevel** | **Number** |  | [optional] 
**department** | **Number** |  | [optional] 
**effectiveDate** | **String** |  | [optional] 
**operationState** | **String** |  | [optional] 


