# WindwardSystemFiveApi.GetPartsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetPartsResult]**](GetPartsResult.md) |  | [optional] 


