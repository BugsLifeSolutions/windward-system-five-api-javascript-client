# WindwardSystemFiveApi.GetRentalRatesDataRate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rateName** | **String** |  | [optional] 
**rateValue** | **Number** |  | [optional] 


