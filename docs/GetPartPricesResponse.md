# WindwardSystemFiveApi.GetPartPricesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetPartPricesResult]**](GetPartPricesResult.md) |  | [optional] 


