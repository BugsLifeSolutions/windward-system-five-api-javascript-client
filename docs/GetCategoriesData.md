# WindwardSystemFiveApi.GetCategoriesData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_number** | **Number** |  | [optional] 
**catName** | **String** |  | [optional] 
**catType** | **String** |  | [optional] 
**catTypeName** | **String** |  | [optional] 


