# WindwardSystemFiveApi.ListPartsParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partUniques** | **[String]** |  | [optional] 
**categories** | **[String]** |  | [optional] 


