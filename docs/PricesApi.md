# WindwardSystemFiveApi.PricesApi

All URIs are relative to *https://api.applianceshack.com:2143/datasnap/rest/TServerMethodsWebAPI*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getContractPrices**](PricesApi.md#getContractPrices) | **POST** /Get_Contract_Prices | 
[**getMasterContract**](PricesApi.md#getMasterContract) | **POST** /Get_Master_Contract | 
[**getPartPriceChanges**](PricesApi.md#getPartPriceChanges) | **POST** /Get_Part_Price_Changes | 
[**getPartPrices**](PricesApi.md#getPartPrices) | **POST** /Get_Part_Prices | 


<a name="getContractPrices"></a>
# **getContractPrices**
> GetContractPricesResponse getContractPrices(getContractPrices)



Accepts a GetContractPrices_Params object. If successful, returns an GetContractPrices_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.PricesApi();

var getContractPrices = new WindwardSystemFiveApi.GetContractPricesParams(); // GetContractPricesParams | Customer Unique Number and Part Uniques


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getContractPrices(getContractPrices, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getContractPrices** | [**GetContractPricesParams**](GetContractPricesParams.md)| Customer Unique Number and Part Uniques | 

### Return type

[**GetContractPricesResponse**](GetContractPricesResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMasterContract"></a>
# **getMasterContract**
> GetMasterContractResponse getMasterContract(getMasterContract)



Accepts a MasterContract_Params object. If successful, returns an GetMasterContract_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.PricesApi();

var getMasterContract = new WindwardSystemFiveApi.MasterContractParams(); // MasterContractParams | Customer Unique Number


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getMasterContract(getMasterContract, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getMasterContract** | [**MasterContractParams**](MasterContractParams.md)| Customer Unique Number | 

### Return type

[**GetMasterContractResponse**](GetMasterContractResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPartPriceChanges"></a>
# **getPartPriceChanges**
> GetPartPriceChangesResponse getPartPriceChanges(getPartPriceChanges)



Accepts a Changes_Params object. If successful, returns an GetPartPriceChanges_Response object. (max. 100 Price Schedules)

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.PricesApi();

var getPartPriceChanges = new WindwardSystemFiveApi.ChangesParams(); // ChangesParams | Search parameters


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getPartPriceChanges(getPartPriceChanges, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getPartPriceChanges** | [**ChangesParams**](ChangesParams.md)| Search parameters | 

### Return type

[**GetPartPriceChangesResponse**](GetPartPriceChangesResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPartPrices"></a>
# **getPartPrices**
> GetPartPricesResponse getPartPrices(getPartPrices)



Accepts a PartPrices_Params object. If successful, returns an GetPartPrices_Response object. (max. 100 Price Schedules)

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.PricesApi();

var getPartPrices = new WindwardSystemFiveApi.PartPricesParams(); // PartPricesParams | Search parameters


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getPartPrices(getPartPrices, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getPartPrices** | [**PartPricesParams**](PartPricesParams.md)| Search parameters | 

### Return type

[**GetPartPricesResponse**](GetPartPricesResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

