# WindwardSystemFiveApi.GetPartChangesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetPartChangesResult]**](GetPartChangesResult.md) |  | [optional] 


