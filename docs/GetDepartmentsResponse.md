# WindwardSystemFiveApi.GetDepartmentsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetDepartmentsResult]**](GetDepartmentsResult.md) |  | [optional] 


