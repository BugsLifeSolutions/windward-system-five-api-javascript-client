# WindwardSystemFiveApi.GetMainCategoriesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetMainCategoriesResult]**](GetMainCategoriesResult.md) |  | [optional] 


