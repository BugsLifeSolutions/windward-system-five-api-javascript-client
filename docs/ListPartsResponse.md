# WindwardSystemFiveApi.ListPartsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[ListPartsResult]**](ListPartsResult.md) |  | [optional] 


