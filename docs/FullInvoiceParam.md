# WindwardSystemFiveApi.FullInvoiceParam

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoiceHeader** | [**InvoiceHeader**](InvoiceHeader.md) |  | 
**invoiceLines** | [**[InvoiceLine]**](InvoiceLine.md) |  | 
**invoiceTenders** | [**[InvoiceTender]**](InvoiceTender.md) |  | 
**invoiceShipping** | [**InvoiceAddress**](InvoiceAddress.md) |  | 
**invoiceBilling** | [**InvoiceAddress**](InvoiceAddress.md) |  | 


