# WindwardSystemFiveApi.GetKitsData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kitUnique** | **Number** |  | [optional] 
**mainPartUnique** | **Number** |  | [optional] 
**childPartUnique** | **Number** |  | [optional] 
**isSerial** | **Boolean** |  | [optional] 
**quantity** | **Number** |  | [optional] 


