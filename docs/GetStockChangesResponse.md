# WindwardSystemFiveApi.GetStockChangesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[GetStockChangesResult]**](GetStockChangesResult.md) |  | [optional] 


