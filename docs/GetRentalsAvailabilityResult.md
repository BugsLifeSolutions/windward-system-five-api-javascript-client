# WindwardSystemFiveApi.GetRentalsAvailabilityResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[GetRentalsAvailabilityData]**](GetRentalsAvailabilityData.md) |  | [optional] 
**response** | **String** |  | [optional] 
**reason** | **String** |  | [optional] 
**stopwatch** | **String** |  | [optional] 


