# WindwardSystemFiveApi.TenderTypesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**[TenderTypesResult]**](TenderTypesResult.md) |  | [optional] 


