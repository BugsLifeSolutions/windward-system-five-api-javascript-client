# WindwardSystemFiveApi.SalespeopleResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[SalespeopleData]**](SalespeopleData.md) |  | [optional] 
**response** | **String** |  | [optional] 
**reason** | **String** |  | [optional] 
**stopwatch** | **String** |  | [optional] 


