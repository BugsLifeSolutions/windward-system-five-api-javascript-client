# WindwardSystemFiveApi.GetRentalsAvailabilityData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partUnique** | **Number** |  | [optional] 
**quantityAvailable** | **Number** |  | [optional] 


