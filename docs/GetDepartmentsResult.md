# WindwardSystemFiveApi.GetDepartmentsResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[GetDepartmentsData]**](GetDepartmentsData.md) |  | [optional] 
**response** | **String** |  | [optional] 
**reason** | **String** |  | [optional] 
**stopwatch** | **String** |  | [optional] 


