# WindwardSystemFiveApi.SalespeopleData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**uniqueNumber** | **String** |  | [optional] 


