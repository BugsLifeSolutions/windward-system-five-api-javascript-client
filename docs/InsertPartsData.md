# WindwardSystemFiveApi.InsertPartsData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**part** | [**GetPartsData**](GetPartsData.md) |  | [optional] 
**currentTime** | **Number** |  | [optional] 
**currentDate** | **Date** |  | [optional] 


