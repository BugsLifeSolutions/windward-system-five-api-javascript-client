# WindwardSystemFiveApi.InventoryApi

All URIs are relative to *https://api.applianceshack.com:2143/datasnap/rest/TServerMethodsWebAPI*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addPartImage**](InventoryApi.md#addPartImage) | **POST** /Add_Part_Image | 
[**getCategories**](InventoryApi.md#getCategories) | **GET** /Get_Categories | 
[**getContractPrices**](InventoryApi.md#getContractPrices) | **POST** /Get_Contract_Prices | 
[**getKits**](InventoryApi.md#getKits) | **POST** /Get_Kits | 
[**getMainCategories**](InventoryApi.md#getMainCategories) | **GET** /Get_Main_Categories | 
[**getMasterContract**](InventoryApi.md#getMasterContract) | **POST** /Get_Master_Contract | 
[**getPartChanges**](InventoryApi.md#getPartChanges) | **POST** /Get_Part_Changes | 
[**getPartPriceChanges**](InventoryApi.md#getPartPriceChanges) | **POST** /Get_Part_Price_Changes | 
[**getPartPrices**](InventoryApi.md#getPartPrices) | **POST** /Get_Part_Prices | 
[**getParts**](InventoryApi.md#getParts) | **POST** /Get_Parts | 
[**getStock**](InventoryApi.md#getStock) | **POST** /Get_Stock | 
[**getStockChanges**](InventoryApi.md#getStockChanges) | **POST** /Get_Stock_Changes | 
[**getSupersedingParts**](InventoryApi.md#getSupersedingParts) | **POST** /Get_Superseding_Parts | 
[**getSupersedingPartsChanges**](InventoryApi.md#getSupersedingPartsChanges) | **POST** /Get_Superseding_Parts_Changes | 
[**insertParts**](InventoryApi.md#insertParts) | **POST** /Insert_Parts | 
[**listParts**](InventoryApi.md#listParts) | **POST** /List_Parts | 


<a name="addPartImage"></a>
# **addPartImage**
> AddPartImageResponse addPartImage(addPartImage)



Accepts a AddPartImage_Params object. If successful, returns an AddPartImage_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var addPartImage = new WindwardSystemFiveApi.AddPartImageParams(); // AddPartImageParams | Part Unique Number, Image URL, Image FileName


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.addPartImage(addPartImage, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **addPartImage** | [**AddPartImageParams**](AddPartImageParams.md)| Part Unique Number, Image URL, Image FileName | 

### Return type

[**AddPartImageResponse**](AddPartImageResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCategories"></a>
# **getCategories**
> GetCategoriesResponse getCategories()



Returns a list of Categories.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getCategories(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**GetCategoriesResponse**](GetCategoriesResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getContractPrices"></a>
# **getContractPrices**
> GetContractPricesResponse getContractPrices(getContractPrices)



Accepts a GetContractPrices_Params object. If successful, returns an GetContractPrices_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var getContractPrices = new WindwardSystemFiveApi.GetContractPricesParams(); // GetContractPricesParams | Customer Unique Number and Part Uniques


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getContractPrices(getContractPrices, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getContractPrices** | [**GetContractPricesParams**](GetContractPricesParams.md)| Customer Unique Number and Part Uniques | 

### Return type

[**GetContractPricesResponse**](GetContractPricesResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getKits"></a>
# **getKits**
> GetKitsResponse getKits(getKits)



Accepts a GetKits_Params object. If successful, returns an GetKits_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var getKits = new WindwardSystemFiveApi.GetKitsParams(); // GetKitsParams | Kit Field/Filter params


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getKits(getKits, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getKits** | [**GetKitsParams**](GetKitsParams.md)| Kit Field/Filter params | 

### Return type

[**GetKitsResponse**](GetKitsResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMainCategories"></a>
# **getMainCategories**
> GetMainCategoriesResponse getMainCategories()



Returns a list of Main Categories.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getMainCategories(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**GetMainCategoriesResponse**](GetMainCategoriesResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMasterContract"></a>
# **getMasterContract**
> GetMasterContractResponse getMasterContract(getMasterContract)



Accepts a MasterContract_Params object. If successful, returns an GetMasterContract_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var getMasterContract = new WindwardSystemFiveApi.MasterContractParams(); // MasterContractParams | Customer Unique Number


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getMasterContract(getMasterContract, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getMasterContract** | [**MasterContractParams**](MasterContractParams.md)| Customer Unique Number | 

### Return type

[**GetMasterContractResponse**](GetMasterContractResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPartChanges"></a>
# **getPartChanges**
> GetPartChangesResponse getPartChanges(getPartChanges)



Accepts a Changes_Params object. If successful, returns an GetPartChanges_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var getPartChanges = new WindwardSystemFiveApi.ChangesParams(); // ChangesParams | Search parameters


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getPartChanges(getPartChanges, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getPartChanges** | [**ChangesParams**](ChangesParams.md)| Search parameters | 

### Return type

[**GetPartChangesResponse**](GetPartChangesResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPartPriceChanges"></a>
# **getPartPriceChanges**
> GetPartPriceChangesResponse getPartPriceChanges(getPartPriceChanges)



Accepts a Changes_Params object. If successful, returns an GetPartPriceChanges_Response object. (max. 100 Price Schedules)

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var getPartPriceChanges = new WindwardSystemFiveApi.ChangesParams(); // ChangesParams | Search parameters


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getPartPriceChanges(getPartPriceChanges, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getPartPriceChanges** | [**ChangesParams**](ChangesParams.md)| Search parameters | 

### Return type

[**GetPartPriceChangesResponse**](GetPartPriceChangesResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPartPrices"></a>
# **getPartPrices**
> GetPartPricesResponse getPartPrices(getPartPrices)



Accepts a PartPrices_Params object. If successful, returns an GetPartPrices_Response object. (max. 100 Price Schedules)

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var getPartPrices = new WindwardSystemFiveApi.PartPricesParams(); // PartPricesParams | Search parameters


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getPartPrices(getPartPrices, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getPartPrices** | [**PartPricesParams**](PartPricesParams.md)| Search parameters | 

### Return type

[**GetPartPricesResponse**](GetPartPricesResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getParts"></a>
# **getParts**
> GetPartsResponse getParts(getParts)



Accepts a GetParts_Params object. If successful, returns an GetParts_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var getParts = new WindwardSystemFiveApi.GetPartsParams(); // GetPartsParams | Part Field/Filter params


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getParts(getParts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getParts** | [**GetPartsParams**](GetPartsParams.md)| Part Field/Filter params | 

### Return type

[**GetPartsResponse**](GetPartsResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getStock"></a>
# **getStock**
> GetStockResponse getStock(getStock)



Accepts a GetStock_Params object. If successful, returns an GetStock_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var getStock = new WindwardSystemFiveApi.GetStockParams(); // GetStockParams | List of Part Uniques


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getStock(getStock, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getStock** | [**GetStockParams**](GetStockParams.md)| List of Part Uniques | 

### Return type

[**GetStockResponse**](GetStockResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getStockChanges"></a>
# **getStockChanges**
> GetStockChangesResponse getStockChanges(getStockChanges)



Accepts a Changes_Params object. If successful, returns an GetStockChanges_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var getStockChanges = new WindwardSystemFiveApi.ChangesParams(); // ChangesParams | Search parameters


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getStockChanges(getStockChanges, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getStockChanges** | [**ChangesParams**](ChangesParams.md)| Search parameters | 

### Return type

[**GetStockChangesResponse**](GetStockChangesResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getSupersedingParts"></a>
# **getSupersedingParts**
> GetSupersedingPartsResponse getSupersedingParts(getSupersedingParts)



Accepts a SupersedingParts_Params object. If successful, returns an GetSupersedingParts_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var getSupersedingParts = new WindwardSystemFiveApi.SupersedingPartsParams(); // SupersedingPartsParams | Search parameters


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getSupersedingParts(getSupersedingParts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getSupersedingParts** | [**SupersedingPartsParams**](SupersedingPartsParams.md)| Search parameters | 

### Return type

[**GetSupersedingPartsResponse**](GetSupersedingPartsResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getSupersedingPartsChanges"></a>
# **getSupersedingPartsChanges**
> GetSupersedingPartsChangesResponse getSupersedingPartsChanges(getSupersedingPartsChanges)



Accepts a Changes_Params object. If successful, returns an GetSupersedingPartsChanges_Response object.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var getSupersedingPartsChanges = new WindwardSystemFiveApi.ChangesParams(); // ChangesParams | Search parameters


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getSupersedingPartsChanges(getSupersedingPartsChanges, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getSupersedingPartsChanges** | [**ChangesParams**](ChangesParams.md)| Search parameters | 

### Return type

[**GetSupersedingPartsChangesResponse**](GetSupersedingPartsChangesResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="insertParts"></a>
# **insertParts**
> InsertPartsResponse insertParts(insertParts)



Accepts Parts array parameter with the necessary information to add/update the part(s).

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var insertParts = new WindwardSystemFiveApi.InsertPartsParams(); // InsertPartsParams | Search parameters


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.insertParts(insertParts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **insertParts** | [**InsertPartsParams**](InsertPartsParams.md)| Search parameters | 

### Return type

[**InsertPartsResponse**](InsertPartsResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="listParts"></a>
# **listParts**
> ListPartsResponse listParts(listParts)



Accepts either a list of Part Unique numbers or a list of Categories and returns parts information based on the parameters.

### Example
```javascript
var WindwardSystemFiveApi = require('windward_system_five_api');
var defaultClient = WindwardSystemFiveApi.ApiClient.instance;

// Configure HTTP basic authorization: BasicAuth
var BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

var apiInstance = new WindwardSystemFiveApi.InventoryApi();

var listParts = new WindwardSystemFiveApi.ListPartsParams(); // ListPartsParams | Search parameters


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.listParts(listParts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **listParts** | [**ListPartsParams**](ListPartsParams.md)| Search parameters | 

### Return type

[**ListPartsResponse**](ListPartsResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

