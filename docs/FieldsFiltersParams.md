# WindwardSystemFiveApi.FieldsFiltersParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields** | **[String]** |  | 
**filters** | [**[WebAPIFilter]**](WebAPIFilter.md) |  | 


