# WindwardSystemFiveApi.InsertRetailCustomerResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[InsertRetailCustomerData]**](InsertRetailCustomerData.md) |  | [optional] 
**response** | **String** |  | [optional] 
**reason** | **String** |  | [optional] 
**stopwatch** | **String** |  | [optional] 


