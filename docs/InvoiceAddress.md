# WindwardSystemFiveApi.InvoiceAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstName** | **String** |  | 
**lastName** | **String** |  | 
**address** | **String** |  | 
**city** | **String** |  | 
**stateProvince** | **String** |  | 
**country** | **String** |  | 
**zipPostal** | **String** |  | 


