/**
 * Windward System Five API
 * Provides Web API functions to Windward System Five
 *
 * OpenAPI spec version: v0.1.3
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/FieldsFiltersParams', 'model/GetSuppliersResponse', 'model/ListSuppliersResponse', 'model/SupplierUniquesParam'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/FieldsFiltersParams'), require('../model/GetSuppliersResponse'), require('../model/ListSuppliersResponse'), require('../model/SupplierUniquesParam'));
  } else {
    // Browser globals (root is window)
    if (!root.WindwardSystemFiveApi) {
      root.WindwardSystemFiveApi = {};
    }
    root.WindwardSystemFiveApi.SuppliersApi = factory(root.WindwardSystemFiveApi.ApiClient, root.WindwardSystemFiveApi.FieldsFiltersParams, root.WindwardSystemFiveApi.GetSuppliersResponse, root.WindwardSystemFiveApi.ListSuppliersResponse, root.WindwardSystemFiveApi.SupplierUniquesParam);
  }
}(this, function(ApiClient, FieldsFiltersParams, GetSuppliersResponse, ListSuppliersResponse, SupplierUniquesParam) {
  'use strict';

  /**
   * Suppliers service.
   * @module api/SuppliersApi
   * @version v0.1.3
   */

  /**
   * Constructs a new SuppliersApi. 
   * @alias module:api/SuppliersApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the getSuppliers operation.
     * @callback module:api/SuppliersApi~getSuppliersCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetSuppliersResponse} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Accepts a FieldsFilters_Params object. If successful, returns an GetSuppliers_Response object.
     * @param {module:model/FieldsFiltersParams} getSuppliers Part Field/Filter params
     * @param {module:api/SuppliersApi~getSuppliersCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetSuppliersResponse}
     */
    this.getSuppliers = function(getSuppliers, callback) {
      var postBody = getSuppliers;

      // verify the required parameter 'getSuppliers' is set
      if (getSuppliers === undefined || getSuppliers === null) {
        throw new Error("Missing the required parameter 'getSuppliers' when calling getSuppliers");
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['BasicAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = GetSuppliersResponse;

      return this.apiClient.callApi(
        '/Get_Suppliers', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the listSuppliers operation.
     * @callback module:api/SuppliersApi~listSuppliersCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ListSuppliersResponse} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Accepts a SupplierUniques_Param object. If successful, returns an ListSuppliers_Response object.
     * @param {module:model/SupplierUniquesParam} listSuppliers Supplier Unique Number(s)
     * @param {module:api/SuppliersApi~listSuppliersCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/ListSuppliersResponse}
     */
    this.listSuppliers = function(listSuppliers, callback) {
      var postBody = listSuppliers;

      // verify the required parameter 'listSuppliers' is set
      if (listSuppliers === undefined || listSuppliers === null) {
        throw new Error("Missing the required parameter 'listSuppliers' when calling listSuppliers");
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['BasicAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = ListSuppliersResponse;

      return this.apiClient.callApi(
        '/List_Suppliers', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
