/**
 * Windward System Five API
 * Provides Web API functions to Windward System Five
 *
 * OpenAPI spec version: v0.1.3
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.WindwardSystemFiveApi) {
      root.WindwardSystemFiveApi = {};
    }
    root.WindwardSystemFiveApi.ListSuppliersData = factory(root.WindwardSystemFiveApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The ListSuppliersData model module.
   * @module model/ListSuppliersData
   * @version v0.1.3
   */

  /**
   * Constructs a new <code>ListSuppliersData</code>.
   * @alias module:model/ListSuppliersData
   * @class
   */
  var exports = function() {
    var _this = this;





















































  };

  /**
   * Constructs a <code>ListSuppliersData</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/ListSuppliersData} obj Optional instance to populate.
   * @return {module:model/ListSuppliersData} The populated <code>ListSuppliersData</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('SupplierUnique')) {
        obj['SupplierUnique'] = ApiClient.convertToType(data['SupplierUnique'], 'Number');
      }
      if (data.hasOwnProperty('SupplierName')) {
        obj['SupplierName'] = ApiClient.convertToType(data['SupplierName'], 'String');
      }
      if (data.hasOwnProperty('Addr_1')) {
        obj['Addr_1'] = ApiClient.convertToType(data['Addr_1'], 'String');
      }
      if (data.hasOwnProperty('Addr_2')) {
        obj['Addr_2'] = ApiClient.convertToType(data['Addr_2'], 'String');
      }
      if (data.hasOwnProperty('City')) {
        obj['City'] = ApiClient.convertToType(data['City'], 'String');
      }
      if (data.hasOwnProperty('State')) {
        obj['State'] = ApiClient.convertToType(data['State'], 'String');
      }
      if (data.hasOwnProperty('Country')) {
        obj['Country'] = ApiClient.convertToType(data['Country'], 'String');
      }
      if (data.hasOwnProperty('Postal')) {
        obj['Postal'] = ApiClient.convertToType(data['Postal'], 'String');
      }
      if (data.hasOwnProperty('Phone_1')) {
        obj['Phone_1'] = ApiClient.convertToType(data['Phone_1'], 'String');
      }
      if (data.hasOwnProperty('Phone_2')) {
        obj['Phone_2'] = ApiClient.convertToType(data['Phone_2'], 'String');
      }
      if (data.hasOwnProperty('Phone_3')) {
        obj['Phone_3'] = ApiClient.convertToType(data['Phone_3'], 'String');
      }
      if (data.hasOwnProperty('CurrencyCode')) {
        obj['CurrencyCode'] = ApiClient.convertToType(data['CurrencyCode'], 'Number');
      }
      if (data.hasOwnProperty('Duty')) {
        obj['Duty'] = ApiClient.convertToType(data['Duty'], 'String');
      }
      if (data.hasOwnProperty('FedTax')) {
        obj['FedTax'] = ApiClient.convertToType(data['FedTax'], 'String');
      }
      if (data.hasOwnProperty('AutoDiscount')) {
        obj['AutoDiscount'] = ApiClient.convertToType(data['AutoDiscount'], 'String');
      }
      if (data.hasOwnProperty('DueDays')) {
        obj['DueDays'] = ApiClient.convertToType(data['DueDays'], 'Number');
      }
      if (data.hasOwnProperty('PriceSchedule')) {
        obj['PriceSchedule'] = ApiClient.convertToType(data['PriceSchedule'], 'Number');
      }
      if (data.hasOwnProperty('ContractDate')) {
        obj['ContractDate'] = ApiClient.convertToType(data['ContractDate'], 'Date');
      }
      if (data.hasOwnProperty('Salesman')) {
        obj['Salesman'] = ApiClient.convertToType(data['Salesman'], 'Number');
      }
      if (data.hasOwnProperty('CSType')) {
        obj['CSType'] = ApiClient.convertToType(data['CSType'], 'String');
      }
      if (data.hasOwnProperty('Terms')) {
        obj['Terms'] = ApiClient.convertToType(data['Terms'], 'String');
      }
      if (data.hasOwnProperty('Interest')) {
        obj['Interest'] = ApiClient.convertToType(data['Interest'], 'Number');
      }
      if (data.hasOwnProperty('TaxStatus')) {
        obj['TaxStatus'] = ApiClient.convertToType(data['TaxStatus'], 'String');
      }
      if (data.hasOwnProperty('TaxNumber')) {
        obj['TaxNumber'] = ApiClient.convertToType(data['TaxNumber'], 'String');
      }
      if (data.hasOwnProperty('BankInfo')) {
        obj['BankInfo'] = ApiClient.convertToType(data['BankInfo'], 'String');
      }
      if (data.hasOwnProperty('ShipNo')) {
        obj['ShipNo'] = ApiClient.convertToType(data['ShipNo'], 'Number');
      }
      if (data.hasOwnProperty('SearchContact')) {
        obj['SearchContact'] = ApiClient.convertToType(data['SearchContact'], 'Number');
      }
      if (data.hasOwnProperty('CreditLimit')) {
        obj['CreditLimit'] = ApiClient.convertToType(data['CreditLimit'], 'Number');
      }
      if (data.hasOwnProperty('StandingPO')) {
        obj['StandingPO'] = ApiClient.convertToType(data['StandingPO'], 'String');
      }
      if (data.hasOwnProperty('POExpiryDate')) {
        obj['POExpiryDate'] = ApiClient.convertToType(data['POExpiryDate'], 'Date');
      }
      if (data.hasOwnProperty('POMaximumValue')) {
        obj['POMaximumValue'] = ApiClient.convertToType(data['POMaximumValue'], 'Number');
      }
      if (data.hasOwnProperty('GSTExempt')) {
        obj['GSTExempt'] = ApiClient.convertToType(data['GSTExempt'], 'String');
      }
      if (data.hasOwnProperty('Department')) {
        obj['Department'] = ApiClient.convertToType(data['Department'], 'Number');
      }
      if (data.hasOwnProperty('TaxCode')) {
        obj['TaxCode'] = ApiClient.convertToType(data['TaxCode'], 'String');
      }
      if (data.hasOwnProperty('Number')) {
        obj['Number'] = ApiClient.convertToType(data['Number'], 'String');
      }
      if (data.hasOwnProperty('DisNv')) {
        obj['DisNv'] = ApiClient.convertToType(data['DisNv'], 'Number');
      }
      if (data.hasOwnProperty('DisStat')) {
        obj['DisStat'] = ApiClient.convertToType(data['DisStat'], 'Number');
      }
      if (data.hasOwnProperty('RetailType')) {
        obj['RetailType'] = ApiClient.convertToType(data['RetailType'], 'String');
      }
      if (data.hasOwnProperty('Foreign')) {
        obj['Foreign'] = ApiClient.convertToType(data['Foreign'], 'String');
      }
      if (data.hasOwnProperty('LastVisit')) {
        obj['LastVisit'] = ApiClient.convertToType(data['LastVisit'], 'Date');
      }
      if (data.hasOwnProperty('TaxBit1')) {
        obj['TaxBit1'] = ApiClient.convertToType(data['TaxBit1'], 'String');
      }
      if (data.hasOwnProperty('TaxBit2')) {
        obj['TaxBit2'] = ApiClient.convertToType(data['TaxBit2'], 'String');
      }
      if (data.hasOwnProperty('TaxBit3')) {
        obj['TaxBit3'] = ApiClient.convertToType(data['TaxBit3'], 'String');
      }
      if (data.hasOwnProperty('TaxBit4')) {
        obj['TaxBit4'] = ApiClient.convertToType(data['TaxBit4'], 'String');
      }
      if (data.hasOwnProperty('TaxBit5')) {
        obj['TaxBit5'] = ApiClient.convertToType(data['TaxBit5'], 'String');
      }
      if (data.hasOwnProperty('TaxBit6')) {
        obj['TaxBit6'] = ApiClient.convertToType(data['TaxBit6'], 'String');
      }
      if (data.hasOwnProperty('TaxBit7')) {
        obj['TaxBit7'] = ApiClient.convertToType(data['TaxBit7'], 'String');
      }
      if (data.hasOwnProperty('TaxBit8')) {
        obj['TaxBit8'] = ApiClient.convertToType(data['TaxBit8'], 'String');
      }
      if (data.hasOwnProperty('EMail')) {
        obj['EMail'] = ApiClient.convertToType(data['EMail'], 'String');
      }
      if (data.hasOwnProperty('Web')) {
        obj['Web'] = ApiClient.convertToType(data['Web'], 'String');
      }
      if (data.hasOwnProperty('Password')) {
        obj['Password'] = ApiClient.convertToType(data['Password'], 'String');
      }
      if (data.hasOwnProperty('ECommerce')) {
        obj['ECommerce'] = ApiClient.convertToType(data['ECommerce'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {Number} SupplierUnique
   */
  exports.prototype['SupplierUnique'] = undefined;
  /**
   * @member {String} SupplierName
   */
  exports.prototype['SupplierName'] = undefined;
  /**
   * @member {String} Addr_1
   */
  exports.prototype['Addr_1'] = undefined;
  /**
   * @member {String} Addr_2
   */
  exports.prototype['Addr_2'] = undefined;
  /**
   * @member {String} City
   */
  exports.prototype['City'] = undefined;
  /**
   * @member {String} State
   */
  exports.prototype['State'] = undefined;
  /**
   * @member {String} Country
   */
  exports.prototype['Country'] = undefined;
  /**
   * @member {String} Postal
   */
  exports.prototype['Postal'] = undefined;
  /**
   * @member {String} Phone_1
   */
  exports.prototype['Phone_1'] = undefined;
  /**
   * @member {String} Phone_2
   */
  exports.prototype['Phone_2'] = undefined;
  /**
   * @member {String} Phone_3
   */
  exports.prototype['Phone_3'] = undefined;
  /**
   * @member {Number} CurrencyCode
   */
  exports.prototype['CurrencyCode'] = undefined;
  /**
   * @member {String} Duty
   */
  exports.prototype['Duty'] = undefined;
  /**
   * @member {String} FedTax
   */
  exports.prototype['FedTax'] = undefined;
  /**
   * @member {String} AutoDiscount
   */
  exports.prototype['AutoDiscount'] = undefined;
  /**
   * @member {Number} DueDays
   */
  exports.prototype['DueDays'] = undefined;
  /**
   * @member {Number} PriceSchedule
   */
  exports.prototype['PriceSchedule'] = undefined;
  /**
   * @member {Date} ContractDate
   */
  exports.prototype['ContractDate'] = undefined;
  /**
   * @member {Number} Salesman
   */
  exports.prototype['Salesman'] = undefined;
  /**
   * @member {String} CSType
   */
  exports.prototype['CSType'] = undefined;
  /**
   * @member {String} Terms
   */
  exports.prototype['Terms'] = undefined;
  /**
   * @member {Number} Interest
   */
  exports.prototype['Interest'] = undefined;
  /**
   * @member {String} TaxStatus
   */
  exports.prototype['TaxStatus'] = undefined;
  /**
   * @member {String} TaxNumber
   */
  exports.prototype['TaxNumber'] = undefined;
  /**
   * @member {String} BankInfo
   */
  exports.prototype['BankInfo'] = undefined;
  /**
   * @member {Number} ShipNo
   */
  exports.prototype['ShipNo'] = undefined;
  /**
   * @member {Number} SearchContact
   */
  exports.prototype['SearchContact'] = undefined;
  /**
   * @member {Number} CreditLimit
   */
  exports.prototype['CreditLimit'] = undefined;
  /**
   * @member {String} StandingPO
   */
  exports.prototype['StandingPO'] = undefined;
  /**
   * @member {Date} POExpiryDate
   */
  exports.prototype['POExpiryDate'] = undefined;
  /**
   * @member {Number} POMaximumValue
   */
  exports.prototype['POMaximumValue'] = undefined;
  /**
   * @member {String} GSTExempt
   */
  exports.prototype['GSTExempt'] = undefined;
  /**
   * @member {Number} Department
   */
  exports.prototype['Department'] = undefined;
  /**
   * @member {String} TaxCode
   */
  exports.prototype['TaxCode'] = undefined;
  /**
   * @member {String} Number
   */
  exports.prototype['Number'] = undefined;
  /**
   * @member {Number} DisNv
   */
  exports.prototype['DisNv'] = undefined;
  /**
   * @member {Number} DisStat
   */
  exports.prototype['DisStat'] = undefined;
  /**
   * @member {String} RetailType
   */
  exports.prototype['RetailType'] = undefined;
  /**
   * @member {String} Foreign
   */
  exports.prototype['Foreign'] = undefined;
  /**
   * @member {Date} LastVisit
   */
  exports.prototype['LastVisit'] = undefined;
  /**
   * @member {String} TaxBit1
   */
  exports.prototype['TaxBit1'] = undefined;
  /**
   * @member {String} TaxBit2
   */
  exports.prototype['TaxBit2'] = undefined;
  /**
   * @member {String} TaxBit3
   */
  exports.prototype['TaxBit3'] = undefined;
  /**
   * @member {String} TaxBit4
   */
  exports.prototype['TaxBit4'] = undefined;
  /**
   * @member {String} TaxBit5
   */
  exports.prototype['TaxBit5'] = undefined;
  /**
   * @member {String} TaxBit6
   */
  exports.prototype['TaxBit6'] = undefined;
  /**
   * @member {String} TaxBit7
   */
  exports.prototype['TaxBit7'] = undefined;
  /**
   * @member {String} TaxBit8
   */
  exports.prototype['TaxBit8'] = undefined;
  /**
   * @member {String} EMail
   */
  exports.prototype['EMail'] = undefined;
  /**
   * @member {String} Web
   */
  exports.prototype['Web'] = undefined;
  /**
   * @member {String} Password
   */
  exports.prototype['Password'] = undefined;
  /**
   * @member {String} ECommerce
   */
  exports.prototype['ECommerce'] = undefined;



  return exports;
}));


