/**
 * Windward System Five API
 * Provides Web API functions to Windward System Five
 *
 * OpenAPI spec version: v0.1.3
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.WindwardSystemFiveApi) {
      root.WindwardSystemFiveApi = {};
    }
    root.WindwardSystemFiveApi.InvoiceHeader = factory(root.WindwardSystemFiveApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The InvoiceHeader model module.
   * @module model/InvoiceHeader
   * @version v0.1.3
   */

  /**
   * Constructs a new <code>InvoiceHeader</code>.
   * @alias module:model/InvoiceHeader
   * @class
   * @param invoiceDate {String} 
   * @param invoiceType {String} 
   * @param invoiceDepartment {Number} 
   * @param invoiceBookMonth {String} 
   * @param invoiceCustomer {Number} 
   */
  var exports = function(invoiceDate, invoiceType, invoiceDepartment, invoiceBookMonth, invoiceCustomer) {
    var _this = this;

    _this['InvoiceDate'] = invoiceDate;
    _this['InvoiceType'] = invoiceType;

    _this['InvoiceDepartment'] = invoiceDepartment;
    _this['InvoiceBookMonth'] = invoiceBookMonth;
    _this['InvoiceCustomer'] = invoiceCustomer;


  };

  /**
   * Constructs a <code>InvoiceHeader</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/InvoiceHeader} obj Optional instance to populate.
   * @return {module:model/InvoiceHeader} The populated <code>InvoiceHeader</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('InvoiceDate')) {
        obj['InvoiceDate'] = ApiClient.convertToType(data['InvoiceDate'], 'String');
      }
      if (data.hasOwnProperty('InvoiceType')) {
        obj['InvoiceType'] = ApiClient.convertToType(data['InvoiceType'], 'String');
      }
      if (data.hasOwnProperty('InvoiceSubType')) {
        obj['InvoiceSubType'] = ApiClient.convertToType(data['InvoiceSubType'], 'String');
      }
      if (data.hasOwnProperty('InvoiceDepartment')) {
        obj['InvoiceDepartment'] = ApiClient.convertToType(data['InvoiceDepartment'], 'Number');
      }
      if (data.hasOwnProperty('InvoiceBookMonth')) {
        obj['InvoiceBookMonth'] = ApiClient.convertToType(data['InvoiceBookMonth'], 'String');
      }
      if (data.hasOwnProperty('InvoiceCustomer')) {
        obj['InvoiceCustomer'] = ApiClient.convertToType(data['InvoiceCustomer'], 'Number');
      }
      if (data.hasOwnProperty('InvoiceSalesman')) {
        obj['InvoiceSalesman'] = ApiClient.convertToType(data['InvoiceSalesman'], 'Number');
      }
      if (data.hasOwnProperty('InvoiceComment')) {
        obj['InvoiceComment'] = ApiClient.convertToType(data['InvoiceComment'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {String} InvoiceDate
   */
  exports.prototype['InvoiceDate'] = undefined;
  /**
   * @member {String} InvoiceType
   */
  exports.prototype['InvoiceType'] = undefined;
  /**
   * @member {String} InvoiceSubType
   */
  exports.prototype['InvoiceSubType'] = undefined;
  /**
   * @member {Number} InvoiceDepartment
   */
  exports.prototype['InvoiceDepartment'] = undefined;
  /**
   * @member {String} InvoiceBookMonth
   */
  exports.prototype['InvoiceBookMonth'] = undefined;
  /**
   * @member {Number} InvoiceCustomer
   */
  exports.prototype['InvoiceCustomer'] = undefined;
  /**
   * @member {Number} InvoiceSalesman
   */
  exports.prototype['InvoiceSalesman'] = undefined;
  /**
   * @member {String} InvoiceComment
   */
  exports.prototype['InvoiceComment'] = undefined;



  return exports;
}));


