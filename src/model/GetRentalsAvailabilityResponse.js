/**
 * Windward System Five API
 * Provides Web API functions to Windward System Five
 *
 * OpenAPI spec version: v0.1.3
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/GetRentalsAvailabilityResult'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./GetRentalsAvailabilityResult'));
  } else {
    // Browser globals (root is window)
    if (!root.WindwardSystemFiveApi) {
      root.WindwardSystemFiveApi = {};
    }
    root.WindwardSystemFiveApi.GetRentalsAvailabilityResponse = factory(root.WindwardSystemFiveApi.ApiClient, root.WindwardSystemFiveApi.GetRentalsAvailabilityResult);
  }
}(this, function(ApiClient, GetRentalsAvailabilityResult) {
  'use strict';




  /**
   * The GetRentalsAvailabilityResponse model module.
   * @module model/GetRentalsAvailabilityResponse
   * @version v0.1.3
   */

  /**
   * Constructs a new <code>GetRentalsAvailabilityResponse</code>.
   * @alias module:model/GetRentalsAvailabilityResponse
   * @class
   */
  var exports = function() {
    var _this = this;


  };

  /**
   * Constructs a <code>GetRentalsAvailabilityResponse</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/GetRentalsAvailabilityResponse} obj Optional instance to populate.
   * @return {module:model/GetRentalsAvailabilityResponse} The populated <code>GetRentalsAvailabilityResponse</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('result')) {
        obj['result'] = ApiClient.convertToType(data['result'], [GetRentalsAvailabilityResult]);
      }
    }
    return obj;
  }

  /**
   * @member {Array.<module:model/GetRentalsAvailabilityResult>} result
   */
  exports.prototype['result'] = undefined;



  return exports;
}));


