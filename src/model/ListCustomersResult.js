/**
 * Windward System Five API
 * Provides Web API functions to Windward System Five
 *
 * OpenAPI spec version: v0.1.3
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ListCustomersData'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./ListCustomersData'));
  } else {
    // Browser globals (root is window)
    if (!root.WindwardSystemFiveApi) {
      root.WindwardSystemFiveApi = {};
    }
    root.WindwardSystemFiveApi.ListCustomersResult = factory(root.WindwardSystemFiveApi.ApiClient, root.WindwardSystemFiveApi.ListCustomersData);
  }
}(this, function(ApiClient, ListCustomersData) {
  'use strict';




  /**
   * The ListCustomersResult model module.
   * @module model/ListCustomersResult
   * @version v0.1.3
   */

  /**
   * Constructs a new <code>ListCustomersResult</code>.
   * @alias module:model/ListCustomersResult
   * @class
   */
  var exports = function() {
    var _this = this;





  };

  /**
   * Constructs a <code>ListCustomersResult</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/ListCustomersResult} obj Optional instance to populate.
   * @return {module:model/ListCustomersResult} The populated <code>ListCustomersResult</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('Results')) {
        obj['Results'] = ApiClient.convertToType(data['Results'], [ListCustomersData]);
      }
      if (data.hasOwnProperty('Response')) {
        obj['Response'] = ApiClient.convertToType(data['Response'], 'String');
      }
      if (data.hasOwnProperty('Reason')) {
        obj['Reason'] = ApiClient.convertToType(data['Reason'], 'String');
      }
      if (data.hasOwnProperty('Stopwatch')) {
        obj['Stopwatch'] = ApiClient.convertToType(data['Stopwatch'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {Array.<module:model/ListCustomersData>} Results
   */
  exports.prototype['Results'] = undefined;
  /**
   * @member {String} Response
   */
  exports.prototype['Response'] = undefined;
  /**
   * @member {String} Reason
   */
  exports.prototype['Reason'] = undefined;
  /**
   * @member {String} Stopwatch
   */
  exports.prototype['Stopwatch'] = undefined;



  return exports;
}));


