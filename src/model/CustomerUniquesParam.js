/**
 * Windward System Five API
 * Provides Web API functions to Windward System Five
 *
 * OpenAPI spec version: v0.1.3
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.WindwardSystemFiveApi) {
      root.WindwardSystemFiveApi = {};
    }
    root.WindwardSystemFiveApi.CustomerUniquesParam = factory(root.WindwardSystemFiveApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The CustomerUniquesParam model module.
   * @module model/CustomerUniquesParam
   * @version v0.1.3
   */

  /**
   * Constructs a new <code>CustomerUniquesParam</code>.
   * @alias module:model/CustomerUniquesParam
   * @class
   * @param customerUniques {Array.<String>} 
   */
  var exports = function(customerUniques) {
    var _this = this;

    _this['CustomerUniques'] = customerUniques;
  };

  /**
   * Constructs a <code>CustomerUniquesParam</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/CustomerUniquesParam} obj Optional instance to populate.
   * @return {module:model/CustomerUniquesParam} The populated <code>CustomerUniquesParam</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('CustomerUniques')) {
        obj['CustomerUniques'] = ApiClient.convertToType(data['CustomerUniques'], ['String']);
      }
    }
    return obj;
  }

  /**
   * @member {Array.<String>} CustomerUniques
   */
  exports.prototype['CustomerUniques'] = undefined;



  return exports;
}));


