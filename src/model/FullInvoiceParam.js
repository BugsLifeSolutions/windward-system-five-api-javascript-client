/**
 * Windward System Five API
 * Provides Web API functions to Windward System Five
 *
 * OpenAPI spec version: v0.1.3
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/InvoiceAddress', 'model/InvoiceHeader', 'model/InvoiceLine', 'model/InvoiceTender'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./InvoiceAddress'), require('./InvoiceHeader'), require('./InvoiceLine'), require('./InvoiceTender'));
  } else {
    // Browser globals (root is window)
    if (!root.WindwardSystemFiveApi) {
      root.WindwardSystemFiveApi = {};
    }
    root.WindwardSystemFiveApi.FullInvoiceParam = factory(root.WindwardSystemFiveApi.ApiClient, root.WindwardSystemFiveApi.InvoiceAddress, root.WindwardSystemFiveApi.InvoiceHeader, root.WindwardSystemFiveApi.InvoiceLine, root.WindwardSystemFiveApi.InvoiceTender);
  }
}(this, function(ApiClient, InvoiceAddress, InvoiceHeader, InvoiceLine, InvoiceTender) {
  'use strict';




  /**
   * The FullInvoiceParam model module.
   * @module model/FullInvoiceParam
   * @version v0.1.3
   */

  /**
   * Constructs a new <code>FullInvoiceParam</code>.
   * @alias module:model/FullInvoiceParam
   * @class
   * @param invoiceHeader {module:model/InvoiceHeader} 
   * @param invoiceLines {Array.<module:model/InvoiceLine>} 
   * @param invoiceTenders {Array.<module:model/InvoiceTender>} 
   * @param invoiceShipping {module:model/InvoiceAddress} 
   * @param invoiceBilling {module:model/InvoiceAddress} 
   */
  var exports = function(invoiceHeader, invoiceLines, invoiceTenders, invoiceShipping, invoiceBilling) {
    var _this = this;

    _this['InvoiceHeader'] = invoiceHeader;
    _this['InvoiceLines'] = invoiceLines;
    _this['InvoiceTenders'] = invoiceTenders;
    _this['InvoiceShipping'] = invoiceShipping;
    _this['InvoiceBilling'] = invoiceBilling;
  };

  /**
   * Constructs a <code>FullInvoiceParam</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/FullInvoiceParam} obj Optional instance to populate.
   * @return {module:model/FullInvoiceParam} The populated <code>FullInvoiceParam</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('InvoiceHeader')) {
        obj['InvoiceHeader'] = InvoiceHeader.constructFromObject(data['InvoiceHeader']);
      }
      if (data.hasOwnProperty('InvoiceLines')) {
        obj['InvoiceLines'] = ApiClient.convertToType(data['InvoiceLines'], [InvoiceLine]);
      }
      if (data.hasOwnProperty('InvoiceTenders')) {
        obj['InvoiceTenders'] = ApiClient.convertToType(data['InvoiceTenders'], [InvoiceTender]);
      }
      if (data.hasOwnProperty('InvoiceShipping')) {
        obj['InvoiceShipping'] = InvoiceAddress.constructFromObject(data['InvoiceShipping']);
      }
      if (data.hasOwnProperty('InvoiceBilling')) {
        obj['InvoiceBilling'] = InvoiceAddress.constructFromObject(data['InvoiceBilling']);
      }
    }
    return obj;
  }

  /**
   * @member {module:model/InvoiceHeader} InvoiceHeader
   */
  exports.prototype['InvoiceHeader'] = undefined;
  /**
   * @member {Array.<module:model/InvoiceLine>} InvoiceLines
   */
  exports.prototype['InvoiceLines'] = undefined;
  /**
   * @member {Array.<module:model/InvoiceTender>} InvoiceTenders
   */
  exports.prototype['InvoiceTenders'] = undefined;
  /**
   * @member {module:model/InvoiceAddress} InvoiceShipping
   */
  exports.prototype['InvoiceShipping'] = undefined;
  /**
   * @member {module:model/InvoiceAddress} InvoiceBilling
   */
  exports.prototype['InvoiceBilling'] = undefined;



  return exports;
}));


